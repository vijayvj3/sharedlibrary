def call(){
  withCredentials([usernamePassword(credentialsId: 'ansible', passwordVariable: 'pass', usernameVariable: 'userId')]) {
      sh 'sshpass -p ${pass} ssh -v -o StrictHostKeyChecking=no vjd@172.31.7.235\
     "ansible-playbook -i /home/vjd/playbooks/hosts /home/vjd/playbooks/playbook.yml --extra-vars "ansible_sudo_pass=${pass}"\"'
  }
}